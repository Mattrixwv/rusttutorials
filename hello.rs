//Tutorials/hello.rs
//Matthew Ellison
// Created: 06-06-20
//Modified: 06-06-20
//This is a hello world program for rust


fn main(){
	//Basic printing
	println!("Hello World");
	let x = 5;
	println!("'x' = {}", x);
}
